import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:polars_app/utils/utils.dart';

class IconTitle extends StatelessWidget {
  const IconTitle({Key key, this.title, this.icon}) : super(key: key);
  final title;
  final icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: Container(
          margin: EdgeInsets.only(top: Adapt.px(34), bottom: Adapt.px(34)),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage(this.icon),
                    width: Adapt.px(48),
                    height: Adapt.px(48),
                  ),
                  Text(
                    this.title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12.0,
                    ),
                  ),
                ],
              ),
              Positioned(
                right: 0,
                child: Container(
                  height: Adapt.px(64),
                  width: Adapt.px(1),
                  color: Adapt.hexColor(0xd7d7d7, alpha: 0.26),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './utils/utils.dart';
import './components/icon_title.dart';
import './app.dart';

void main() {
  runApp(MyApp());
  if (Platform.isAndroid) {
// 以下两行 设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ).copyWith(statusBarBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '融智汇',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.white,
      ),
      home: App(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    
  }
}

Widget _buildDocuments() {
  var lists = [
    [
      {
        "title": '费用申请单',
        "icon": 'assets/images/3.0x/expense-application.png',
      },
      {
        "title": '报账单',
        "icon": 'assets/images/3.0x/baozhangdan.png',
      },
      {
        "title": '借款单',
        "icon": 'assets/images/3.0x/jiekuandan.png',
      },
      {
        "title": '我的发票',
        "icon": 'assets/images/3.0x/fapiao.png',
      }
    ],
    [
      {
        "title": '差旅申请',
        "icon": 'assets/images/3.0x/chailvshenqing.png',
      },
      {
        "title": '审批',
        "icon": 'assets/images/3.0x/shenpi.png',
      },
      {
        "title": '我的账本',
        "icon": 'assets/images/3.0x/zhangben.png',
      },
      {
        "title": '更多',
        "icon": 'assets/images/3.0x/gengduo.png',
      }
    ],
  ];

  return Padding(
    padding: EdgeInsets.only(
      top: Adapt.px(60),
      left: Adapt.px(30),
      right: Adapt.px(30),
    ),
    child: Container(
      child: Column(
        children: <Widget>[
          Row(
              children: lists[0]
                  .map((item) => _buildGridItem(item['title'], item['icon']))
                  .toList()),
          SizedBox(
            height: Adapt.px(50),
          ),
          Row(
              children: lists[1]
                  .map((item) => _buildGridItem(item['title'], item['icon']))
                  .toList()),
        ],
      ),
    ),
  );
}

Widget _buildGridItem(title, icon) {
  return Expanded(
    child: Column(
      children: <Widget>[
        Image.asset(
          icon,
          height: Adapt.px(62),
          width: Adapt.px(64),
        ),
        SizedBox(
          height: Adapt.px(20),
        ),
        Text(
          title,
          style: TextStyle(
            color: Color(0xff666666),
            fontSize: 13,
          ),
        ),
      ],
    ),
  );
}

Widget _buildNav() {
  return Padding(
    padding: EdgeInsets.only(left: Adapt.px(30), right: Adapt.px(30)),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: Container(
        height: Adapt.px(160),
        color: Color.fromARGB(
          255,
          50,
          119,
          255,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconTitle(
              title: '我要出差',
              icon: 'assets/images/3.0x/travel.png',
            ),
            IconTitle(
              title: '我要报账',
              icon: 'assets/images/3.0x/baozhang.png',
            ),
            IconTitle(
              title: '待办事项',
              icon: 'assets/images/3.0x/daiban.png',
            ),
            IconTitle(
              title: '我的费用',
              icon: 'assets/images/3.0x/feiyong.png',
            ),
          ],
        ),
      ),
    ),
  );
}

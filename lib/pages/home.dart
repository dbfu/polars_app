import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:polars_app/components/icon_title.dart';
import 'package:polars_app/utils/utils.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(
            color: Color.fromARGB(255, 51, 51, 51),
            fontSize: 18.0,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          IconButton(
            icon: Image.asset('assets/images/3.0x/scanning.png'),
            onPressed: () {},
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          _buildNav(),
          _buildDocuments(),
        ],
      ),
    );
  }
}

Widget _buildDocuments() {
  var lists = [
    [
      {
        "title": '费用申请单',
        "icon": 'assets/images/3.0x/expense-application.png',
      },
      {
        "title": '报账单',
        "icon": 'assets/images/3.0x/baozhangdan.png',
      },
      {
        "title": '借款单',
        "icon": 'assets/images/3.0x/jiekuandan.png',
      },
      {
        "title": '我的发票',
        "icon": 'assets/images/3.0x/fapiao.png',
      }
    ],
    [
      {
        "title": '差旅申请',
        "icon": 'assets/images/3.0x/chailvshenqing.png',
      },
      {
        "title": '审批',
        "icon": 'assets/images/3.0x/shenpi.png',
      },
      {
        "title": '我的账本',
        "icon": 'assets/images/3.0x/zhangben.png',
      },
      {
        "title": '更多',
        "icon": 'assets/images/3.0x/gengduo.png',
      }
    ],
  ];

  return Padding(
    padding: EdgeInsets.only(
      top: Adapt.px(60),
      left: Adapt.px(30),
      right: Adapt.px(30),
    ),
    child: Container(
      child: Column(
        children: <Widget>[
          Row(
            children: lists[0]
                .map((item) => _buildGridItem(item['title'], item['icon']))
                .toList(),
          ),
          SizedBox(
            height: Adapt.px(50),
          ),
          Row(
              children: lists[1]
                  .map((item) => _buildGridItem(item['title'], item['icon']))
                  .toList()),
        ],
      ),
    ),
  );
}

Widget _buildGridItem(title, icon) {
  return Expanded(
    child: Column(
      children: <Widget>[
        Image.asset(
          icon,
          height: Adapt.px(62),
          width: Adapt.px(64),
        ),
        SizedBox(
          height: Adapt.px(20),
        ),
        Text(
          title,
          style: TextStyle(
            color: Color(0xff666666),
            fontSize: 13,
          ),
        ),
      ],
    ),
  );
}

Widget _buildNav() {
  return Padding(
    padding: EdgeInsets.only(left: Adapt.px(30), right: Adapt.px(30)),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: Container(
        height: Adapt.px(160),
        color: Color.fromARGB(
          255,
          50,
          119,
          255,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconTitle(
              title: '我要出差',
              icon: 'assets/images/3.0x/travel.png',
            ),
            IconTitle(
              title: '我要报账',
              icon: 'assets/images/3.0x/baozhang.png',
            ),
            IconTitle(
              title: '待办事项',
              icon: 'assets/images/3.0x/daiban.png',
            ),
            IconTitle(
              title: '我的费用',
              icon: 'assets/images/3.0x/feiyong.png',
            ),
          ],
        ),
      ),
    ),
  );
}
